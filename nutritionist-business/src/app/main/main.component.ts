import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  routeBooking() {
    window.open('https://my.practicebetter.io/#/5fa827522a90290764379099/bookings?step=services');
  }

}
