import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  linksDisabled = true;

  constructor() { }

  ngOnInit(): void {
  }

  routeBooking() {
    window.open('https://my.practicebetter.io/#/5fa827522a90290764379099/bookings?step=services');
  }
}
